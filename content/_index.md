+++
insert_anchor_links = "left"
title = "Main Page"
+++

{% crt() %}
```
     ,__,
(/__/\oo/\__(/
  _/\/__\/\_
   _/    \_   
```
{% end %}

# Whoami

He/Him, born in mid 90s, (origin Samara, Russia — now living in Portugal).
- Graduated from school in 2010.
- Engineer of Information Systems and Technologies 2015.
- Master of Information Systems 2016.

## Experience

- 2011-2013: Web Designer [Freelance] - Web Sites development using Joomla (PHP/MySql/HTML/CSS/JS)
- 2013-2017: Java Developer [Netcracker](https://www.netcracker.com/) - Distributed Web Services development for TOMS & MANO Telecom Solutions (Java/Spring/Docker/SQL/NoSQL)
- 2017-2019: Lead Java Developer [EPAM](https://www.epam.com/) - Team Lead in Well Being and Financial projects (Java/Spring/K8S/SQL/NoSQL)
- 2019-2020: Resource Developement Manager [EPAM](https://www.epam.com/) - Head of EPAM Training Center (Java/Management)
- 2020-2020: Rust Developer [Soramitsu](https://soramitsu.co.jp/) - Development of distributed ledger [Iroha](https://github.com/hyperledger/iroha) (Rust)
- 2020-2021: IT Consultant working with [Sprout (now Elemy)](https://elemy.com/) (Rust/K8S)
- 2021-2022: Senior Developer at Proprietary project for High-Frequency trading domain (Rust)
- 2022-2025: Software Engineer at [Yat](https://y.at) 💜 building modern web services in Rust 🦀 (Rust/K8s)
- 2025-...: Software Engineer at **ToBeDisclosed** 💜 building modern distributed ledger in Rust 🦀 (Rust)

More information about my career can be found on [LinkedIn](https://www.linkedin.com/in/bshn/).

## Skills 

I'm an expert in Web Technologies and have a lot of production experience with developing Microservices and other distributed systems. 
I use my soft skills to lead teams, establish effective communications with stakeholders and present projects to customers or community.
I'm in love with DevOps culture, Cloud-native approach and total Automation of software-development pipeline.

SKILL | Can Understand | Can Develop | Can Teach | Can Contribute 
----- | -------------- | ----------- | --------- | --------------
Rust  | ★ | ★ | ★ | ☆
K8S   | ★ | ★ | ☆ | ☐
SQL   | ★ | ★ | ☆ | ☆
Web3  | ★ | ★ | ☆ | ☆

☐ - goal
☆ - not an expert
★ - experienced

## Interests

I am a cat person 🐈. Interested and very enthusiastic about `Rust` 🦀, see my talks about it:
- [Rust vs Java](https://www.youtube.com/watch?v=yfQhMxZ0JYc) (Russian)
- [Rust or There and Back again](https://www.youtube.com/watch?v=90mxemR3A3I) (Russian)

Occasionally do some contributions to Open-Source software projects via [GitHub](https://github.com/humb1t) and [GitLab](https://gitlab.com/humb1t).

Founder, exAdministrator and exDriver of home-town [local IT Community](https://sitc.community).

Certified Technical Interviewer, sk8boarder, ex-champion in sailing.

## Contacts

Feel free to reach me on [LinkedIn](https://www.linkedin.com/in/bshn/). 
