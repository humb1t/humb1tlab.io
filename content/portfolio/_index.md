+++
insert_anchor_links = "left"
title = "Portfolio"
+++

{% crt() %}
```
💼
```
{% end %}

# Portfolio

List of projects that you can look at to see my code.

Project | Year of development | Short Description | Stack of Technologies | Status
------- | ------------------- | ----------------- | --------------------- | ------
ChatRustLet | 2025 | Rust multiplex I/O based chat implementation | MIO | WIP
OkxLivro    | 2025 | Rust Client library for interactions with OKX API | Tokio, Tungstenite | Released

The up to date list of all projects in Portfolio can be found in [Gitlab Group](https://gitlab.com/personal1196/portfolio).

## Contacts

Feel free to reach me on [LinkedIn](https://www.linkedin.com/in/bshn/).

