+++
authors = ["Nikita Bishonen"]
title = "Rust Types - Taxonomy"
description = "Графическое представление таксономии типов в Раст."
date = 2025-02-19
[taxonomies]
tags = ["Rust", "Types"]
[extra]
archive = false
+++

Привет! Сегодня я делюсь своими мыслями о таксономии типов в Раст. Это первая версия таксономии и поста, буду благодарен за любые идеи по её улучшению.

# Таксономия

> **Таксоно́мия** (от др.-греч. τάξις — строй, порядок и νόμος — закон) — учение о принципах и практике классификации и систематизации сложноорганизованных иерархически соотносящихся сущностей.

Я захотел украсить свой кабинет графической иллюстрацией "таксономии" системы типов данных, схожей с той, что использует Раст. К сожалению мои поиски не принесли желаемый результат и я решил сделать свою иллюстрацию и делюсь ею с вами в этой публикации.

# Типы

## Теории
Теорий существует множество, некоторые из которых обусловлены историей развития науки о вычислениях, другие имеют действительную практическую ценность и теоретическое обоснование. Приведу ссылки на две популярные теории из обеих категорий:

- [Intuitionistic type theory](https://en.wikipedia.org/wiki/Intuitionistic_type_theory)
- [Homotopy Type Theory](https://ryansandford.github.io/projects/hott/)

Но не буду пытаться вдаваться в детали, поскольку понимаю их со скрипом, что уж говорить о способности их объяснить.

![HoTT](/images/2025_02_19_tax_hott.png)

## Теории типов и Раст
Собственно основным вопросом, ответ на который я искал - теория типов стоящая за Раст и её проекция на типы, которые мы используем при написании кода на Раст. 
Не я один задавался таким вопросом: https://users.rust-lang.org/t/practical-intro-to-type-theory/18204/5
Также есть небольшой доклад на эту тему: https://av.tib.eu/media/52178 
"Ментальная модель": https://ia0.github.io/unsafe-mental-model/type-theory.html
А также интересный пост: https://www.kurtlawrence.info/blog/category-theory-with-rust-pt1
Но все они имеют много букв и не имеют "всеобъемлющего" графика, чтобы я повесил его на стену.

# Таксономия Раст Типов

![Taxonomy](/images/2025_02_19_tax_main.png)

В итоге я, на основании своих познаний в теории, составил первый вариант такой таксономии. Я очень люблю использовать деревья (особенно бинарные), для выстраивания иерархий, мне кажется что так легче навигироваться и понимать их суть. Сама по себе визуализация и является конечно целью этой публикации, но для интересующихся и неискушённых читателей я привожу некоторые пояснения и ссылки ниже.

## П-тип

[Тип произведение](https://ru.wikipedia.org/wiki/%D0%A2%D0%B8%D0%BF-%D0%BF%D1%80%D0%BE%D0%B8%D0%B7%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5) ни дать, ни взять. Мы компануем несколько типов в один, получаем произведение возможных значений этих типов как новое множество значений. Какие типы в Раст его реализуют?

- Кортеж: [Tuple](https://doc.rust-lang.org/stable/book/ch03-02-data-types.html#the-tuple-type)
- Структура: [Struct](https://doc.rust-lang.org/stable/book/ch05-00-structs.html)
    - Кортеж-Структура: [Tuple Struct](https://doc.rust-lang.org/stable/book/ch05-01-defining-structs.html#using-tuple-structs-without-named-fields-to-create-different-types)
    - Единичная структура: [Unit-Like Struct](https://doc.rust-lang.org/stable/book/ch05-01-defining-structs.html#unit-like-structs-without-any-fields)
- Массив: [Array](https://doc.rust-lang.org/stable/book/ch03-02-data-types.html#the-array-type)
    - Срез: [Slice](https://doc.rust-lang.org/stable/book/ch04-03-slices.html)

## Σ-тип

[Тип-сумма](https://ru.wikipedia.org/wiki/%D0%A2%D0%B8%D0%BF-%D1%81%D1%83%D0%BC%D0%BC%D0%B0) в отличии от П-типа, тип-сумма даёт более строгий контроль над возможными значениями используемых типов.

- Перечисление: [Tagged Union](https://doc.rust-lang.org/stable/book/ch06-00-enums.html)
- Опцион: [Option](https://doc.rust-lang.org/stable/book/ch06-01-defining-an-enum.html#the-option-enum-and-its-advantages-over-null-values)
- Результат: [Result](https://doc.rust-lang.org/stable/book/ch09-02-recoverable-errors-with-result.html)

## Типы-указатели

Раст имеет возможности для работы с памятью, поэтому язык содержит в себе и ссылки и указатели. Нас же интересуют только "высокоуровневые" концепции.

- Ссылки: [References](https://doc.rust-lang.org/stable/book/ch04-02-references-and-borrowing.html)
![Three tables: the table for s contains only a pointer to the table
for s1. The table for s1 contains the stack data for s1 and points to the
string data on the heap.](https://doc.rust-lang.org/stable/book/img/trpl04-06.svg)
- Умные указатели
    - Коробка: Аллоцирует значение в куче. [Box](https://doc.rust-lang.org/stable/book/ch15-01-box.html#using-boxt-to-point-to-data-on-the-heap)
    - Rc/Arc: Счётчики ссылок. [Reference counters](https://doc.rust-lang.org/stable/book/ch15-04-rc.html#rct-the-reference-counted-smart-pointer)
    - Ref/RefMut + RefCell: Заимствование на уровне исполнения программы. [Runtime borrowing](https://doc.rust-lang.org/stable/book/ch15-05-interior-mutability.html)

## Другие типы

- Функции: Не относятся к текущей версии таксономии, поэтому приведены лишь постольку-поскольку.
    - Замыкания: [Closures](https://doc.rust-lang.org/stable/book/ch13-01-closures.html)
    - FnOnce: [Documentation](https://doc.rust-lang.org/std/ops/trait.FnOnce.html)
    - FnMut: [Documentation](https://doc.rust-lang.org/std/ops/trait.FnMut.html)
    - Fn: [Documentation](https://doc.rust-lang.org/std/ops/trait.Fn.html)
- Объекты типажи: [Trait Objects](https://doc.rust-lang.org/stable/book/ch17-02-trait-objects.html#using-trait-objects-that-allow-for-values-of-different-types)
- Фантомные типы: [Phantom Data](https://doc.rust-lang.org/nomicon/phantom-data.html)

